package projectileSim;

public class ProjectileV3 implements Projectile {
	private double radius, mass;
	private Vector position, velocity;
	private Vector gravity;
	private Vector externalForce;
	private double dragCoefficient;
	
	public final double MAX_SIM_TIME = 100;
	public final double RESOLUTION = 0.1;
	
	public ProjectileV3(double radius, double mass, Vector gravity, Vector externalForce, double dragCoefficient) {
		this.radius = radius;
		this.mass = mass;
		this.gravity = gravity;
		this.externalForce = externalForce;
		this.dragCoefficient = dragCoefficient;
	}
	
	@Override
	public Vector fire(Vector pos, Vector initialVelocity) {
		this.position = pos;
		this.velocity = initialVelocity;
		
		double time = 0;
		
		System.out.println("Beginning simulation with the following conditions [DETAIL: " + RESOLUTION + "]");
		System.out.println("Initial Position(m): " + this.position);
		System.out.println("Initial Velocity(m/s): " + this.velocity);
		System.out.println("Radius(m):" + this.radius + " Mass(kg):" + this.mass);
		System.out.println("Gravity(m/s/s): " + this.gravity);
		System.out.println("External Force(m/s/s): " + this.externalForce);
		System.out.println("Drag Coefficient: " + this.dragCoefficient);
		System.out.println("-------------------------------\n\n");
		
		while(this.position.getZ() >= 0 && time < MAX_SIM_TIME) {
			//Calculate drag force
			Vector dragForce = this.velocity.multiplyBy(this.velocity);
			dragForce = dragForce.multiplyBy(Math.PI*this.radius*this.radius);
			dragForce = dragForce.multiplyBy(0.00119);
			dragForce = dragForce.multiplyBy(this.dragCoefficient);
			dragForce = dragForce.multiplyBy(-1);
			
			//Calculate acceleration
			Vector acceleration = this.gravity.multiplyBy(this.mass);
			acceleration = acceleration.add(this.externalForce);
			acceleration = acceleration.add(dragForce);
			acceleration = acceleration.multiplyBy(1.0/this.mass);
			
			//Display stats
			System.out.println("Time(s): " + time);
			System.out.println("Position(m): " + this.position);
			System.out.println("Velocity(m/s): " + this.velocity);
			System.out.println("Acceleration(m/s/s): " + acceleration);
			System.out.println("\n\n");
			
			//Update velocity and position
			this.velocity = this.velocity.add(acceleration.multiplyBy(RESOLUTION));
			this.position = this.position.add(this.velocity.multiplyBy(RESOLUTION));
			
			time += RESOLUTION;
		}
		
		System.out.println("Time(s): " + time);
		System.out.println("Position(m): " + this.position);
		System.out.println("Velocity(m/s): " + this.velocity);
		System.out.println("\n\n");
		
		if(this.position.getZ() < 0) {
			System.out.println("Simulation ended when projectile hit the ground");
		}else {
			System.out.println("Simulation ended before projectile hit the ground");
		}
		
		return new Vector((double)Math.round(this.position.getX() * 100000d) / 100000d, 
				(double)Math.round(this.position.getY() * 100000d) / 100000d, 
				(double)Math.round(this.position.getZ() * 100000d) / 100000d);
	}
}
