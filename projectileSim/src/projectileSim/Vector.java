package projectileSim;

public class Vector {
	private double x,y,z;
	
	public Vector() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	public Vector(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector(double theta, double alpha) { //assume magnitude is 1
		this.x = Math.cos(theta)*Math.sin(alpha);
		this.y = Math.cos(theta)*Math.cos(alpha);
		this.z = Math.sin(theta);
	}
	
	public Vector add(Vector other) {
		return new Vector(this.x + other.x, this.y + other.y, this.z + other.z);
	}
	
	public Vector subtract(Vector other) {
		return new Vector(this.x - other.x, this.y - other.y, this.z - other.z);
	}
	
	public Vector multiplyBy(double m) {
		return new Vector(this.x*m, this.y*m, this.z*m);
	}
	
	public Vector multiplyBy(Vector other) {
		return new Vector(this.x * other.x, this.y * other.y, this.z * other.z);
	}
	
	public double magnitude() {
		return Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2));
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	public boolean equals(Vector other) {
		return (this.x==other.getX())&&(this.y==other.getY())&&(this.z==other.getZ());
	}
	
	public String toString() {
		return "x:" + this.x + " y:" + this.y + " z:" + this.z;
	}
}
