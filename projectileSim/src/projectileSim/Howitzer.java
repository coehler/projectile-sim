package projectileSim;

public class Howitzer {
	private double barrelPitch, barrelYaw;
	private Vector barrelPos;
	private double initialVelocity;
	private Projectile projectile;
	
	public Projectile getProjectile() {
		return projectile;
	}
	
	public void setProjectile(Projectile projectile) {
		this.projectile = projectile;
	}
	
	public void setBarrelPitch(double barrelPitch) {
		this.barrelPitch = barrelPitch;
	}
	
	public void setBarrelYaw(double barrelYaw) {
		this.barrelYaw = barrelYaw;
	}
	
	public void setBarrelPos(Vector barrelPos) {
		this.barrelPos = barrelPos;
	}
	
	public void setInitialVelocity(double initialVelocity) {
		this.initialVelocity = initialVelocity;
	}
	
	//Fire the projectile, passing in the starting position and initial velocity
	public Vector fire() {
		return this.projectile.fire(this.barrelPos, new Vector(this.barrelPitch, this.barrelYaw).multiplyBy(this.initialVelocity));
	}
}
