package projectileSim;

public interface Projectile {
	public Vector fire(Vector pos, Vector initialVelocity);
}
