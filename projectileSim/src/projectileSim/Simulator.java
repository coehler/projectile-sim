package projectileSim;

public class Simulator {
	public static void main(String[] args) {
		Howitzer h = new Howitzer();
		h.setBarrelPitch(1);
		h.setBarrelYaw(1);
		h.setInitialVelocity(10);
		h.setBarrelPos(new Vector(0,0,0));
		h.setProjectile(new ProjectileV3(1, 1, new Vector(0,0,-9.81), new Vector(5, 5, 0), 0.5));
		
		System.out.println(h.fire());
	}
}
