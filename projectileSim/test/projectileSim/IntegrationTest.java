package projectileSim;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class IntegrationTest {

	final static Vector GRAVITY = new Vector(0,0,-9.81);
	
	@Test
	void test1() {
		Howitzer h = new Howitzer();
		h.setBarrelPos(new Vector(0,0,-0.1));
		h.setProjectile(new ProjectileV3(1,2,GRAVITY, new Vector(1,2,3), 1));
		
		Vector output= h.fire();
				
		assertTrue(output.getX()== 0 && output.getY()==0 && output.getZ()== -0.1);
	}

	@Test
	void test2() {
		Howitzer h = new Howitzer();
		h.setBarrelPos(new Vector(0,0,1));
		h.setProjectile(new ProjectileV3(1,2,GRAVITY, new Vector(1,2,3), 1));
		
		Vector output= h.fire();
		
		assertTrue(output.getX()== 0.075 && output.getY()== 0.14999 && output.getZ()== -0.24715 );
	}
	
}
