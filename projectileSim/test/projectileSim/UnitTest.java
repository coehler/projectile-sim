package projectileSim;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UnitTest {
	
	final static Vector GRAVITY = new Vector(0,0,-9.81);

	@Test
	void fireProjectileV1_1() {
		Projectile p = new ProjectileV1(1,3,GRAVITY);
		Vector initialPosition = new Vector(1,1,1);
		Vector initialVelocity = new Vector(20,20,20);
		Vector sol = new Vector(83,83,-1.4641);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	@Test
	void fireProjectileV1_2() {
		Projectile p = new ProjectileV1(2,2,GRAVITY);
		Vector initialPosition = new Vector(0,0,-0.0001);
		Vector initialVelocity = new Vector(0,0,0);
		Vector sol = new Vector(0,0,-0.0001);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	@Test
	void fireProjectileV1_3() {
		Projectile p = new ProjectileV1(3,1,GRAVITY);
		Vector initialPosition = new Vector(9999,-9999,50);
		Vector initialVelocity = new Vector(3,4,500);
		Vector sol = new Vector(10299.3,-9598.6,902.7519);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	
	@Test
	void fireProjectileV2_1() {
		Vector externalForce = new Vector(1,1,1);
		Projectile p = new ProjectileV2(1,3,GRAVITY,externalForce);
		Vector initialPosition = new Vector(1,1,1);
		Vector initialVelocity = new Vector(20,20,20);
		Vector sol = new Vector(88.01,88.01,-0.5743);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	@Test
	void fireProjectileV2_2() {
		Vector externalForce = new Vector(-1,-1,-1);
		Projectile p = new ProjectileV2(2,2,GRAVITY,externalForce);
		Vector initialPosition = new Vector(0,0,-0.0001);
		Vector initialVelocity = new Vector(0,0,0);
		Vector sol = new Vector(0,0,-0.0001);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	@Test
	void fireProjectileV2_3() {
		Vector externalForce = new Vector(-1,-2,3);
		Projectile p = new ProjectileV2(2,2,GRAVITY,externalForce);
		Vector initialPosition = new Vector(9999,9999,50);
		Vector initialVelocity = new Vector(3,4,500);
		Vector sol = new Vector(7791.795,5384.39,8425.2669);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	
	@Test
	void fireProjectileV3_1() {
		Vector externalForce = new Vector(1,1,1);
		Projectile p = new ProjectileV3(1,3,GRAVITY,externalForce,1);
		Vector initialPosition = new Vector(1,1,1);
		Vector initialVelocity = new Vector(20,20,20);
		Vector sol = new Vector(81.66759,81.66759,-0.07446);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	@Test
	void fireProjectileV3_2() {
		Vector externalForce = new Vector(-1,-1,-1);
		Projectile p = new ProjectileV3(2,2,GRAVITY,externalForce,2);
		Vector initialPosition = new Vector(0,0,-0.0001);
		Vector initialVelocity = new Vector(0,0,0);
		Vector sol = new Vector(0,0,-0.0001);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
	
	@Test
	void fireProjectileV3_3() {
		Vector externalForce = new Vector(-1,-2,3);
		Projectile p = new ProjectileV3(2,2,GRAVITY,externalForce,0.0001);
		Vector initialPosition = new Vector(9999,9999,50);
		Vector initialVelocity = new Vector(3,4,500);
		Vector sol = new Vector(7790.5712,5379.06436,8097.64904);
		assertTrue(p.fire(initialPosition, initialVelocity).equals(sol));
	}
}
