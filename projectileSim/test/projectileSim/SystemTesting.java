package projectileSim;

import static org.junit.jupiter.api.Assertions.*;
import java.io.*;

import org.junit.jupiter.api.Test;

class SystemTesting {

	@Test
	void testCantBeInRestAndFiring() {
		// This program is only in firing mode when the fire function is called.
		// The fire function IS NOT called by any other existing functions in our program.
		// fire has to be explicitly called by the user. This puts the program into the firing state.
		// Hence, our software cannot ever be in both states simultaneously

		// Therefore, this test is an auto pass
		return;
	}

	// Graph Coverage - Node Coverage
	@Test
	void testStateRestToFiringToRest() {
		final ByteArrayOutputStream outstream = new ByteArrayOutputStream();
		final PrintStream out = System.out;
		System.setOut(new PrintStream(outstream));
		
		Howitzer h = new Howitzer();
		h.setBarrelPitch(30);
		h.setBarrelYaw(40);
		h.setInitialVelocity(1);
		h.setBarrelPos(new Vector(0,0,0));
		h.setProjectile(new ProjectileV3(1, 1, new Vector(0,0,-9.81), new Vector(5, 5, 0), 0.5));
		
		assertDoesNotThrow(() -> h.fire()); // added check to ensure no exceptions aren't thrown
		
		String actual = outstream.toString();
		System.setOut(out);
		
		assert(actual.contains("Beginning simulation")); // means the function went from rest to firing
		assert(actual.contains("Simulation ended")); // means the function went from firing to rest
	}
}
