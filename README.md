![banner](/documentation/banner/HowitzerSim.png)
## Group Members
- Mikayla Peterson (200425538)
- Cameron Oehler (200384643)
- Ria Chevli (200416701)

## Problem Specification
A Howitzer company wants a way to teach soldiers about kinematics and dynamics. Soldiers have an incredibly critical job to perform and knowing about kinematics and dynamics would help them do that job better. This project aims to develop a simple tool to teach soldiers about kinematics and dynamics.

## Design Requirements
### Objectives
- To build a working Howitzer artillery simulator in order to simulate the Howitzer shooting a projectile across an open field
- To be able to take into account multiple factors including barrel position/rotation, radius/mass of the projectile, etc.
- To test the simulator with a variety of testing techniques

### Functions
- Simulate a projectile given different variables (barrel height, barrel rotation, projectile radius, projectile mass, drag, initial speed, external force, gravity, drag coefficient)
- Return the final position of the projectile (when it hits the ground or the time exceeds MAX_SIM_TIME)

### Constraints
1. **Economic Factors** - The cost to operate the simulator should be free.

2. **Reliability** - Thorough testing should be done to minimize the presence of errors. The kinematics and dynamics calculations done by the simulator should accurately reflect the firing of a live projectile. The uptime for the simulator should be high with almost no downtime.

3. **Regulatory Compliance** - Since this application is meant to be used by soldiers in the military, our data should be handled according to DOADs, such as [DAOD 6500-0, Data Management and Analytics](https://www.canada.ca/en/department-national-defence/corporate/policies-standards/defence-administrative-orders-directives/6000-series/6500/6500-0-data-management-and-analytics.html) and [DAOD 6003-0, Information Technology Security](https://www.canada.ca/en/department-national-defence/corporate/policies-standards/defence-administrative-orders-directives/6000-series/6003/6003-0-information-technology-security.html) as well as the [Technical Data Control Regulations](https://laws-lois.justice.gc.ca/eng/regulations/sor-86-345/FullText.html), as published by the Government of Canada. We also need to follow all other Department of National Defence guidelines, rules, and regulations. As a note, if this was not just a university project, all team members would need be certified by the United States and Canada's [Joint Certification Program](https://www.tpsgc-pwgsc.gc.ca/pma-jcp/demande-apply-eng.html) to work on this project.

4. **Ethics** - Since we are simulating the firing of a Howitzer, the source code needs to be protected to prevent it from being used by individuals/organizations/countries with ill intent.

5. **Time** - There is a hard deadline for the project to be completed by August 2nd, 23:59:59.

## Project Management (Gantt Chart)
![gantt chart](documentation/gantt_chart.png)

## Solution Implementation
### MVP Descriptions
| MVP | Accounts for | Testing | Conclusion |
|-|-|-|-|
| 1   | Barrel Position, Barrel Pitch, Barrel Yaw, Initial Velocity, Projectile Radius, Projectile Mass, Gravity | Unit | Works well but is unrealistic. Does not account for air drag nor continuously propelled projectiles. |
| 2   | Everything in MVP 1 as well as an External Force | Unit | Works well but is unrealistic as it does not account for drag. |
| 3   | Everything in MVP 1 and 2 as well as a Drag Coefficient | Unit, Integration, System | Works well but could be further improved with a GUI and the ability to have a changing external force. **This is the best of our MVPs.** |

### Class Diagram
We did not use established design patterns such as the facade or singleton pattern when designing the simulator. We broke the problem into logical pieces and created a vector class to assist with calculations.
![class diagram](documentation/classDiagram/classDiagram.png)

## Testing
### Unit Testing

Unit Testing was done on all three of our MVPs and was focused on the Projectile.fire() function since it is a major function that is critical to the operation of the simulator.
#### Control Flow Graphs
MVP 1 Fire Function Control Flow Graph
![node graph](documentation/testing/unitTesting/fireFunctionNodeV1.png)

MVP 2 Fire Function Control Flow Graph
![node graph](documentation/testing/unitTesting/fireFunctionNodeV2.png)

MVP 3 Fire Function Control Flow Graph
![node graph](documentation/testing/unitTesting/fireFunctionNodeV3.png)

#### Prime Paths
![prime path derivation](documentation/testing/unitTesting/UnitTesting_fire_primePaths-1CROPPED.png)

#### Test Paths and Test Cases
![prime path test cases](documentation/testing/unitTesting/UnitTesting_fire_primePaths-2.png)

### Integration Testing
Integration Testing was done on MVP 3 and it tracks the position variable.
#### Def-Use Graph for the Position Variable
![du graph](documentation/testing/integrationTesting/IntegrationTesting_DU_Graph.png)

#### DU Paths and Test Cases
![du path derivation](documentation/testing/integrationTesting/IntegrationTesting_DU_Paths.png)

### System Testing
System Testing was done on MVP 3. To accomplish this, we extracted a Finite State Machine and used node coverage, a form of graph coverage to test it. We did not use logic coverage since we did not have hardcoded predicates to guard the transition between states. If we did have predicates dictating the transition in our code, we would have used logic coverage.

#### Finite State Machine
![fsm](documentation/testing/systemTesting/finiteStateMachine.png)

#### Test Cases
![fsm test cases](documentation/testing/systemTesting/SystemTesting_test_cases.png)

## Future Work
- Visual representations of the simulator output (i.e. 3D simulators of the actual projectile as well as 2D graphs of velocity and acceleration)
- Allow finer control over what forces can be input (changing forces)
- Incorporate our software into a more interactive firing simulator using a real, unloaded Howitzer. This would make a more comprehensive, hands on, and safe to use training tool.
